using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance;

    [SerializeField] private AudioMixerGroup musicMixerGroup;
    [SerializeField] private AudioMixerGroup soundEffectsMixerGroup;
    [SerializeField] private Sound[] sounds;

    private void Awake()
    {
        Instance = this;
        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.audioClip;
            s.source.loop = s.isLoop;
            s.source.volume = s.volume;

            switch (s.audioTypes)
            {
                case Sound.AudioTypes.soundEffect :
                    s.source.outputAudioMixerGroup = soundEffectsMixerGroup;
                    break;
                
                case Sound.AudioTypes.music:
                    s.source.outputAudioMixerGroup = musicMixerGroup;
                    break;
            }
            if (s.playOnAwake)
            {
                s.source.Play();
            }
        }
      
    }

    public void Play(string clipname)
    {
        Sound s = Array.Find(sounds, dummysound => dummysound.clipName == clipname);
        if (s == null)
        {
            Debug.LogError("Sound:" + clipname + "does NOT exist");
            return;
        }
        s.source.Play();
    }

    public void Stop(string clipname)
    {
        Sound s = Array.Find(sounds, dummysound => dummysound.clipName == clipname);
        if (s == null)
        {
            Debug.LogError("Sound:" + clipname + "does NOT exist");
            return;
        }
        
        s.source.Stop();
    }

    public void UpdateMixerVolume()
    {
        musicMixerGroup.audioMixer.SetFloat("Music Volume", Mathf.Log10(AudioOptionManager.musicVolume) * 20);
        soundEffectsMixerGroup.audioMixer.SetFloat("soundEffects Volume", Mathf.Log10(AudioOptionManager.SFXVolume) * 20);
    }
    
    
}
