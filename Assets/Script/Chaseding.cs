using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class Chaseding : MonoBehaviour
{
    private NavMeshAgent enemy;
    public GameObject Player;
    public float enemyDistanceRun = 4.0f;
    
    void Start()
    {
        enemy = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        float distance = Vector3.Distance(transform.position, Player.transform.position);

        if (distance < enemyDistanceRun)
        {
            Vector3 disToPlayer = transform.position - Player.transform.position;
            Vector3 newPos = transform.position - disToPlayer;

            enemy.SetDestination(newPos);
        }
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            SceneManager.LoadScene("Over scene");
        }
    }
    
}
