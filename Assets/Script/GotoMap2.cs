using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GotoMap2 : MonoBehaviour
{
    private void OnTriggerEnter(Collider collision)
       {
           if (collision.gameObject.CompareTag("Player"))
           {
               SceneManager.LoadScene("GamePlay scene2 1");
           }
       }
}
