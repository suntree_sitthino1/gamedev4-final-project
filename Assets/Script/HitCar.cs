using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitCar : MonoBehaviour
{
    void OnCollisionEnter (Collision collisoin)
    {
        if (collisoin.gameObject.tag.Equals("Car"))
        {
            Destroy(collisoin.gameObject);
        }
    }
}
