using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LastCheckpoint : MonoBehaviour
{
    private int scenetocontinue;

    public void ContinueGame()
    {
        scenetocontinue = PlayerPrefs.GetInt("SaveScene");

        if (scenetocontinue != 0)
        {

            SceneManager.LoadScene(scenetocontinue);
        }
        else
        {
            return;
        }

    }
}
