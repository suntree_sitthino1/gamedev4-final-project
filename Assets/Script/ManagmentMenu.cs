using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ManagmentMenu : MonoBehaviour
{
    [SerializeField] Button startButton;
    [SerializeField] Button backButton;
    [SerializeField] Button optionsButton;
    [SerializeField] Button exitButton;
   
    
    void Start()
    {
        {
            startButton.onClick.AddListener (
            delegate{StartButtonClick(startButton);});
        optionsButton.onClick.AddListener (
            delegate{OptionsButtonClick(optionsButton);});
        exitButton.onClick.AddListener (
            delegate{ExitButtonClick(exitButton);});
        backButton.onClick.AddListener(
            delegate {BackButttonClick(backButton); });
        
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartButtonClick(Button button)
    {
        SceneManager.LoadScene("Map01 1");
    }

    public void BackButttonClick(Button button)
    {
        SceneManager.LoadScene("Menu scene");
    }

    public void OptionsButtonClick(Button button)
    {
        
            SceneManager.LoadScene("Option scene");
          
    }

    public void ExitButtonClick(Button button)
    {
        Application.Quit();
    }
    
   
}
