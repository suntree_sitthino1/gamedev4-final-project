using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RandomPoint3 : MonoBehaviour
{
    private NavMeshAgent nma = null;
    private GameObject[] _RandomPoint;
    private int CurrentRandom;
    
    private void Start()
    {
        nma = this.GetComponent<NavMeshAgent>();
        _RandomPoint = GameObject.FindGameObjectsWithTag("RandomPoint3");
        Debug.Log("RandomPoint3 = " + _RandomPoint.Length.ToString());
        
        
    }

    // Update is called once per frame
    private void Update()
    {
        if (nma.hasPath == false)
        {
            CurrentRandom = Random.Range(0, _RandomPoint.Length + 1);
            nma.SetDestination(_RandomPoint[CurrentRandom].transform.position);
            Debug.Log("Moveing to RandomPoint3 " + CurrentRandom.ToString());
        }
        
    }
}