using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TheKiwiCoder;

public class RotateToFineBait : ActionNode
{
    public string TagToBeFinding = "Player";
    public float rotationSpeed = 60;
    private float rotatetime;
    private bool clockWise = false;

    private void RandomRotationParams()
    {
        rotatetime = Random.Range(1, 3);
        float rnd = Random.Range(0, 99);
        if (rnd > 50)
        {
            clockWise = true;
        }
        else
        {
            clockWise = false;
        }
    }
    protected override void OnStart()
    {
        RandomRotationParams();
    }

    protected override void OnStop() {
    }

    protected override State OnUpdate()
    {
        Vector3 rayStart = context.gameObject.transform.position;
        Vector3 rayDir = context.gameObject.transform.forward;
        RaycastHit hitInfo;
        if (Physics.Raycast(rayStart, rayDir, out hitInfo))
        {
            if (hitInfo.transform.gameObject.CompareTag(TagToBeFinding))
            {
                blackboard.moveToPosition = hitInfo.transform.position;
                return State.Success;
            }
        }
        
        float rotationDiractionMuliplier = 1;
        if (clockWise)
        {
            rotationDiractionMuliplier = 1;
        }
        else
        {
            rotationDiractionMuliplier = -1;
        }
        context.gameObject.transform.Rotate(
            new Vector3(0,1,0),
            rotationSpeed*rotationDiractionMuliplier*Time.deltaTime,
            Space.Self);

        rotatetime -= Time.deltaTime;
        if (rotatetime <= 0)
        {
            return State.Failure;
        }
        
        return State.Running;
    }
}
