using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerInformation : MonoBehaviour
{
    public List<Collider> ListColliders = new List<Collider>();

    private void OnTriggerEnter(Collider other)
    {
        ListColliders.Add(other);
    }

    private void OnTriggerExit(Collider other)
    {
        ListColliders.Remove(other);
    }
}
