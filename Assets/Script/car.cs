using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class car : MonoBehaviour
{
    public Transform traget;
    private float t = 0.001f;

    void Start()
    {

    }


    void Update()
    {
        Vector3 a = transform.position;
        Vector3 b = traget.position;
        transform.position = Vector3.Lerp(a, b, t);
    }
    
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            SceneManager.LoadScene("Over scene");
        }
    }

    
}