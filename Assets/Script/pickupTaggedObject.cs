using System.Collections;
using System.Collections.Generic;
using TheKiwiCoder;
using Unity.VisualScripting;
using UnityEngine;

public class pickupTaggedObject : ActionNode
{
    public string tagToBePickedup = "Ball";

    protected override void OnStart()
    {
        
    }
    protected override void OnStop()
    {
        
    }
    protected override State OnUpdate()
    {
        TriggerInformation ti = context.gameObject.GetComponent<TriggerInformation>();

        if (ti == null) return State.Failure;

        foreach (Collider c in ti.ListColliders)
        {
            if (c.gameObject.CompareTag(tagToBePickedup))
            {
                ti.ListColliders.Remove(c);
                Destroy(c.gameObject);
                return State.Success;
            }
            
        }
        

        return State.Failure;
    }
}
