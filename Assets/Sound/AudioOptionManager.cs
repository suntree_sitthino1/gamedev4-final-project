using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioOptionManager : MonoBehaviour
{
    public static float musicVolume { get; private set; }
    public static float SFXVolume { get; private set; }

    [SerializeField] private Text musicSliderText;
    [SerializeField] private Text SFXSliderText;

    public void OnMusicSliderValueChange(float value)
    {
        musicVolume = value;
        musicSliderText.text = ((int)(value * 100)).ToString();
        AudioManager.Instance.UpdateMixerVolume();
    }
    
    public void onSFXSliderValueChange(float value)
    {
        SFXVolume = value;
        SFXSliderText.text = ((int)(value * 100)).ToString();
        AudioManager.Instance.UpdateMixerVolume();
    }
}
